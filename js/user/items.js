var registered_Items = [];

// add_item
function add_item() {
  var id = new Date();
  id = id.getMilliseconds();
  var title = document.getElementById("title").value;
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  var url = document.getElementById("url").value;
  var commentary = document.getElementById("commentary").value;
  var list_of_tags = document.getElementById("list").value;

  var newItem = {
    id: id,
    title: title,
    username: username,
    password: password,
    url: url,
    commentary: commentary,
    list_of_tags: list_of_tags,
  };
  for (var i = 0; i < registered_Items.length; i++) {
    if (title == registered_Items[i].title) {
      alert("Error, item already exists");
      return;
    }
  }
  registered_Items.push(newItem);
  localStorage.setItem("Items", JSON.stringify(registered_Items));

  // Permite insertar fila al final de la tabla
  let table_item_ref = document.getElementById("table_items");
  console.log(table_item_ref);
  let table_item_row = table_item_ref.insertRow(-1);

  // Permite insertar filas a la tabla
  let newTable_Cell_Ref = table_item_row.insertCell(0);
  newTable_Cell_Ref.textContent = id;

  newTable_Cell_Ref = table_item_row.insertCell(1);
  newTable_Cell_Ref.textContent = title;

  newTable_Cell_Ref = table_item_row.insertCell(2);
  newTable_Cell_Ref.textContent = url;

  newTable_Cell_Ref = table_item_row.insertCell(3);
  newTable_Cell_Ref.textContent = commentary;
}

function views() {
  let prueba = 1;
  id_item.innerHTML = prueba;
}

function refresh_table() {
  console.log("Works");
  // Prueba Obtener Datos del localStorage
  if (localStorage.getItem("Items")) {
    let items_Storage = JSON.parse(localStorage.getItem("Items"));
    console.log(items_Storage);
    // Permite insertar fila al final de la tabla

    let table_item_ref = document.getElementById("table_items");
    console.log(table_item_ref);
    let table_item_row = table_item_ref.insertRow(-1);
    for (var i = 0; items_Storage.length; i++) {
      // Permite insertar filas a la tabla
      let newTable_Cell_Ref = table_item_row.insertCell(0);
      newTable_Cell_Ref.textContent = items_Storage[i].id;
    }
  } else {
    console.log("No hay entradas en el LocalStorage");
  }
}
