// Variables Globales

const form_add_item = document.querySelector("#form_add_item");
const registered_items = document.getElementById("registered_items");
let arrayItems = [];

//Funciones

const newItem = (
  title,
  username,
  password,
  url,
  commentary,
  list_item_tags
) => {
  let item = {
    title: title,
    username: username,
    password: password,
    url: url,
    commentary: commentary,
    list_item_tags: list_item_tags,
  };
  arrayItems.push(item);
  return item;
};

const saveArrayItems = () => {
  localStorage.setItem("Items", JSON.stringify(arrayItems));
  viewItem();
};

const viewItem = () => {
  registered_items.innerHTML = "";

  arrayItems = JSON.parse(localStorage.getItem("Items"));
  if (arrayItems === null) {
    arrayItems = [];
  } else {
    arrayItems.forEach((element) => {
      registered_items.innerHTML += `<div class="alert alert-primary" role="alert">
            <b>${element.title}</b>
            <b>${element.username}</b>
            <b>${element.url}</b>
            <b>${element.commentary}</b>
            <b>${element.list_item_tags}</b>
            <span class="float-right"><i class="material-icons float-left mr-2">delete</i></span></div>`;
    });
  }
};

const deleteItem = (delete_item) => {
  let indexArray;
  arrayItems.forEach((element, index) => {
    if (element.delete_item === delete_item) {
      indexArray = index;
    }
  });
  arrayItems.splice(indexArray, 1);
  saveArrayItems();
};

//EventListener

form_add_item.addEventListener("submit", (e) => {
  e.preventDefault();
  let title = document.querySelector("#title").value;
  let username = document.querySelector("#username").value;
  let password = document.querySelector("#password").value;
  let url = document.querySelector("#url").value;
  let commentary = document.querySelector("#commentary").value;
  let list_item_tags = document.querySelector("#list_item_tags").value;

  console.log(username, password, url, commentary, list_item_tags);

  newItem(title, username, password, url, commentary, list_item_tags);
  saveArrayItems();

  form_add_item.reset();
});

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewItem);

registered_items.addEventListener("click", (e) => {
  e.preventDefault();
  if (e.target.innerHTML === "delete") {
    const delete_item = e.path[2].childNodes[1].innerHTML;
    deleteItem(delete_item);
  }
});
