// JS ITEMS
const form_add_item = document.querySelector("#form_add_item");
const registered_items = document.getElementById("table_items");
let arrayItems = [];

const newItem = (
  title,
  username,
  password,
  url,
  commentary,
  list_item_tags
) => {
  let item = {
    id: Date.now(),
    title: title,
    username: username,
    password: password,
    url: url,
    commentary: commentary,
    list_item_tags: list_item_tags,
  };
  arrayItems.push(item);
  return item;
};

const saveArrayItems = () => {
  localStorage.setItem("Items", JSON.stringify(arrayItems));
  viewItem();
};

const viewItem = () => {
  registered_items.innerHTML = "";

  arrayItems = JSON.parse(localStorage.getItem("Items"));
  if (arrayItems === null) {
    arrayItems = [];
  } else {
    arrayItems.forEach((element) => {
      registered_items.innerHTML += `<tr><td>${element.title}</td><td>${element.username}</td><td>${element.password}</td><td>${element.url}</td><td>${element.commentary}</td><td>${element.list_item_tags}</td><td><i class="material-icons float-left mr-2">mode_edit</i><i class="material-icons float-left mr-2">delete</i></td></tr>`;
    });
  }
};

const deleteItem = (item) => {
  let indexArray;
  arrayItems.forEach((element, index) => {
    if (element.title === item) {
      indexArray = index;
      console.log(index);
    }
  });
  arrayItems.splice(indexArray, 1);
  saveArrayItems();
};

const editItem = (item) => {
  let indexArray = arrayItems.findIndex((element) => element.title === item);
  // console.log(indexArray);
  // console.log(arrayItems[indexArray]);
  arrayItems[indexArray].title = "9Do2to";
  saveArrayItems();
};

//EventListener

form_add_item.addEventListener("submit", (e) => {
  e.preventDefault();
  let title = document.querySelector("#title").value;
  let username = document.querySelector("#username").value;
  let password = document.querySelector("#password").value;
  let url = document.querySelector("#url").value;
  let commentary = document.querySelector("#commentary").value;
  let list_item_tags = document.querySelector("#list_item_tags").value;

  console.log(username, password, url, commentary, list_item_tags);

  newItem(title, username, password, url, commentary, list_item_tags);
  saveArrayItems();

  form_add_item.reset();
});

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewItem);

registered_items.addEventListener("click", (e) => {
  e.preventDefault();
  console.log(e.path[2].childNodes[1].innerHTML);
  if (e.target.innerHTML === "delete" || e.target.innerHTML === "mode_edit") {
    let item = e.path[2].childNodes[1].innerHTML;
    if (e.target.innerHTML === "delete") {
      deleteItem(item);
    }
    if (e.target.innerHTML === "mode_edit") {
      editItem(item);
    }
  }
});

// FIN JS ITEM
// _____________________________________

// INICIO JS TAG

// TAG ITEMS
const form_add_tag = document.querySelector("#form_add_tag");
const registered_tags = document.getElementById("table_tags");
const checkboxTag = document.getElementById("checkboxTag");

let arrayTags = [];

const newTag = (tag_name) => {
  let tag = {
    tag_name: tag_name,
  };
  arrayTags.push(tag);
  return tag;
};

const saveArrayTags = () => {
  localStorage.setItem("Tags", JSON.stringify(arrayTags));
  viewTags();
};

const viewTags = () => {
  registered_tags.innerHTML = "";
  checkboxTag.innerHTML = "";

  arrayTags = JSON.parse(localStorage.getItem("Tags"));
  if (arrayTags === null) {
    arrayTags = [];
  } else {
    arrayTags.forEach((element) => {
      registered_tags.innerHTML += `<tr><td>${
        element.tag_name
      }</td><td>${"Hola"}</td><td><i class="material-icons float-left mr-2">mode_edit</i><i class="material-icons float-left mr-2">delete</i></td></tr>`;
    });
  }

  if (arrayTags === null) {
    arrayTags = [];
  } else {
    arrayTags.forEach((element) => {
      checkboxTag.innerHTML += `<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"><label class="form-check-label" for="flexCheckDefault">${element.tag_name}</label>`;
    });
  }
};

const deleteTag = (tag_text) => {
  let indexArray;
  arrayTags.forEach((element, index) => {
    if (element.tag_name === tag_text) {
      indexArray = index;
      console.log(index);
    }
  });
  arrayTags.splice(indexArray, 1);
  saveArrayTags();
};

const editTag = (tag_text) => {
  let indexArray = arrayTags.findIndex(
    (element) => element.tag_name === tag_text
  );
  // console.log(indexArray);
  // console.log(arrayItems[indexArray]);
  arrayTags[indexArray].tag_name = "9Do2to";
  saveArrayTags();
};

//EventListener

form_add_tag.addEventListener("submit", (e) => {
  e.preventDefault();
  let tag_name = document.querySelector("#tag").value;

  console.log(tag_name);

  newTag(tag_name);
  saveArrayTags();

  form_add_tag.reset();
});

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewTags);

registered_tags.addEventListener("click", (e) => {
  e.preventDefault();
  // console.log(e);
  if (e.target.innerHTML === "delete" || e.target.innerHTML === "mode_edit") {
    let tag_text = e.path[2].childNodes[0].innerHTML;
    if (e.target.innerHTML === "delete") {
      deleteTag(tag_text);
    }
    if (e.target.innerHTML === "mode_edit") {
      editTag(tag_text);
    }
  }
});

form_add_item.addEventListener("click", (e) => {
  e.preventDefault();
  console.log(e);
  let text_tag_item = e.path[1].childNodes[1].innerHTML;
  // console.log(text_tag_item);
  console.log(e.target.innerHTML);
});

path[0].labels[0].innerHTML;
