// JS ITEMS
const form_add_item = document.querySelector("#form_add_item");
const registered_items = document.getElementById("table_items");
let arrayItems = [];

const newItem = (title, username, password, url, commentary, arrResultado) => {
  let item = {
    id: Date.now(),
    title: title,
    username: username,
    password: password,
    url: url,
    commentary: commentary,
    list_item_tags: arrResultado,
  };
  arrayItems.push(item);
  return item;
};

const saveArrayItems = () => {
  localStorage.setItem("Items", JSON.stringify(arrayItems));
  viewItem();
};

const viewItem = () => {
  registered_items.innerHTML = "";

  arrayItems = JSON.parse(localStorage.getItem("Items"));
  if (arrayItems === null) {
    arrayItems = [];
  } else {
    arrayItems.forEach((element) => {
      registered_items.innerHTML += `<tr><td>${element.title}</td><td>${
        element.username
      }</td><td>${atob(element.password)}</td><td>${element.url}</td><td>${
        element.commentary
      }</td><td>${
        element.list_item_tags
      }</td><td><i class="material-icons float-left mr-2">mode_edit</i><i class="material-icons float-left mr-2">delete</i></td></tr>`;
    });
  }
};

const deleteItem = (item) => {
  //funcion borrar item de la lista y tabla 
  let indexArray;
  arrayItems.forEach((element, index) => {
    if (element.title === item) {
      indexArray = index;
      console.log(index);
    }
  });
  arrayItems.splice(indexArray, 1);
  saveArrayItems();
};

const editItem = (item) => {
  //funciono de editar item
  let indexArray = arrayItems.findIndex((element) => element.title === item);
  // console.log(indexArray);
  // console.log(arrayItems[indexArray]);

  var menu_edit_items = "Edit Item\n\n";
  menu_edit_items += "1. Title\n";
  menu_edit_items += "2. Username\n";
  menu_edit_items += "3. Password\n";
  menu_edit_items += "4. URL\n";
  menu_edit_items += "5. Commentary\n";
  menu_edit_items += "6. List of Tags\n";
  menu_edit_items += "7. Exit\n";

  var select = 0;
  do {
    //pantalla emergente para la edicion de item dependiendo de numero seleccionado
    select = parseInt(prompt(menu_edit_items));
    switch (select) {
      case 1:
        var edit = prompt("Enter New Tittle", "");
        arrayItems[indexArray].title = edit;
        saveArrayItems();
        break;
      case 2:
        var edit = prompt("Enter New Username", "");
        arrayItems[indexArray].username = edit;
        saveArrayItems();
        break;
      case 3:
        var edit = prompt("Enter New Password", "");
        arrayItems[indexArray].password = edit;
        saveArrayItems();
        break;
      case 4:
        var edit = prompt("Enter New URL", "");
        arrayItems[indexArray].url = edit;
        saveArrayItems();
        break;
      case 5:
        var edit = prompt("Enter New Commentary", "");
        arrayItems[indexArray].commentary = edit;
        saveArrayItems();
        break;
      case 6:
        var edit = prompt("Enter New Commentary", "");
        arrayItems[indexArray].commentary = edit;
        saveArrayItems();
        break;
      case 7:
        break;
    }
  } while (select != 7);
};

const found = () => {
  let indexArray = arrayItems.findIndex((element) => element.title === "q");
  // console.log(indexArray);
  console.log(arrayItems[indexArray]);
  // arrayItems[indexArray].title = "9Do2to";
  // saveArrayItems();
};

//EventListener

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewItem);

registered_items.addEventListener("click", (e) => {
  e.preventDefault();
  console.log(e.path[2].childNodes[0].innerHTML);
  if (e.target.innerHTML === "delete" || e.target.innerHTML === "mode_edit") {
    let item = e.path[2].childNodes[0].innerHTML;
    if (e.target.innerHTML === "delete") {
      var menu_delete_items = "Delete Item\n\n";
      menu_delete_items += "1. Yes\n";
      menu_delete_items += "2. Exit\n";

      let select = parseInt(prompt(menu_delete_items));
      if (select == 1) {
        deleteItem(item);
        found();
        saveArrayItems();
      } else {
      }
    }
    if (e.target.innerHTML === "mode_edit") {
      editItem(item);
      found();
    }
  }
});

// FIN JS ITEM
// _____________________________________

// INICIO JS TAG

// TAG ITEMS
const form_add_tag = document.querySelector("#form_add_tag");
const registered_tags = document.getElementById("table_tags");
const alert_tags = document.getElementById("alert_tags");

let arrayTags = [];

const newTag = (tag_name) => {
  let tag = {
    tag_name: tag_name,
    list_item: "",
    state: false,
  };
  arrayTags.push(tag);
  return tag;
};

const saveArrayTags = () => {
  localStorage.setItem("Tags", JSON.stringify(arrayTags));
  viewTags();
};

const viewTags = () => {
  registered_tags.innerHTML = "";
  alert_tags.innerHTML = "";

  arrayTags = JSON.parse(localStorage.getItem("Tags"));
  if (arrayTags === null) {
    arrayTags = [];
  } else {
    arrayTags.forEach((element) => {
      registered_tags.innerHTML += `<tr><td>${element.tag_name}</td><td>${element.list_item}</td><td><i class="material-icons float-left mr-2">mode_edit</i><i class="material-icons float-left mr-2">delete</i></td></tr>`;
    });
  }

  if (arrayTags === null) {
    arrayTags = [];
  } else {
    arrayTags.forEach((element) => {
      if (element.state) {
        alert_tags.innerHTML += `<div class="alert alert-success" role="alert"><b>${element.tag_name}</b><span class = "float-right"><i class="material-icons">add_box</i></span></div>`;
      } else {
        alert_tags.innerHTML += `<div class="alert alert-danger" role="alert"><b>${element.tag_name}</b><span class = "float-right"><i class="material-icons">add_box</i></span></div>`;
      }
    });
  }
};

const prueba = (text_tag_item) => {
  let tittle = document.querySelector("#title").value;
  registered_tags.innerHTML = "";
  alert_tags.innerHTML = "";

  let indexArray = arrayTags.findIndex(
    (element) => element.tag_name === text_tag_item
  );
  console.log(indexArray);
  console.log(arrayTags[indexArray]);
  arrayTags[indexArray].list_item = tittle;
  saveArrayTags();
};

const deleteTag = (tag_text) => {
  let indexArray;
  arrayTags.forEach((element, index) => {
    if (element.tag_name === tag_text) {
      indexArray = index;
      console.log(index);
    }
  });
  arrayTags.splice(indexArray, 1);
  saveArrayTags();

  let indexArrayItem_Delete = arrayItems.findIndex(
    (element) => element.list_item_tags === tag_text
  );
  arrayItems[indexArrayItem_Delete].list_item_tags = "";
  saveArrayItems();
};

const editTag = (tag_text) => {
  let indexArray = arrayTags.findIndex(
    (element) => element.tag_name === tag_text
  );

  let indexArrayItem = arrayItems.findIndex(
    (element) => element.list_item_tags === tag_text
  );
  // console.log(indexArray);
  // console.log(arrayItems[indexArray]);

  var edit = prompt("Enter New Name", "");
  arrayTags[indexArray].tag_name = edit;
  arrayItems[indexArrayItem].list_item_tags = edit;

  saveArrayTags();
  saveArrayItems();
};

//EventListener

form_add_tag.addEventListener("submit", (e) => {
  e.preventDefault();
  let tag_name = document.querySelector("#tag").value;

  console.log(tag_name);

  newTag(tag_name);
  saveArrayTags();

  form_add_tag.reset();
});

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewTags);

registered_tags.addEventListener("click", (e) => {
  e.preventDefault();
  // console.log(e);
  if (e.target.innerHTML === "delete" || e.target.innerHTML === "mode_edit") {
    let tag_text = e.path[2].childNodes[0].innerHTML;
    if (e.target.innerHTML === "delete") {
      var menu_delete_items = "Delete Item\n\n";
      menu_delete_items += "1. Yes\n";
      menu_delete_items += "2. Exit\n";

      let select = parseInt(prompt(menu_delete_items));
      if (select == 1) {
        deleteTag(tag_text);
        saveArrayTags();
      } else {
      }
    }
    if (e.target.innerHTML === "mode_edit") {
      editTag(tag_text);
    }
  }
});
let listSelectedTags = []; //Almacena los Tags que son seleccionados para el Ítem
let b = new Array();
let h = [];
let arrResultado;

alert_tags.addEventListener("click", (e) => {
  e.preventDefault();
  if (e.target.innerHTML === "add_box") {
    let text_tag_item = e.path[2].childNodes[0].innerHTML;

    let indexArray = arrayTags.findIndex(
      (element) => element.tag_name === text_tag_item
    );
    arrayTags[indexArray].state = true;
    saveArrayTags();

    // __________________________________________________________
    //Código que permite tener varios Tags en un mismo Item.
    // console.log(text_tag_item);

    // let ng = listSelectedTags.unshift(text_tag_item);
    // console.log(ng);
    // console.log(listSelectedTags);

    // posiciones = [0, 1, 2];
    // arrResultado = posiciones.map((i) => listSelectedTags[i]);
    // arrResultado = arrResultado.join();
    // console.log(arrResultado);
    // __________________________________________________________

    form_add_item.addEventListener("submit", (e) => {
      // e.preventDefault();
      let title = document.querySelector("#title").value;
      let username = document.querySelector("#username").value;
      let password = btoa(document.querySelector("#password").value);
      let url = document.querySelector("#url").value;
      let commentary = document.querySelector("#commentary").value;

      let indexArray = arrayTags.findIndex(
        (element) => element.tag_name === text_tag_item
      );
      arrayTags[indexArray].state = false;
      saveArrayTags();

      if (
        title.length != 0 &&
        username.length != 0 &&
        password.length != 0 &&
        url.length != 0
      ) {
        newItem(title, username, password, url, commentary, text_tag_item);
        prueba(text_tag_item);
        saveArrayItems();
      } else {
        alert("Error, Required fields are empty. 😢🤦‍♂️");
      }

      // form_add_item.reset();
    });
  }
});
