const form_add_item = document.querySelector("#form_add_item");
const registered_items = document.getElementById("table_items");
let arrayItems = [];

const newItem = (
  title,
  username,
  password,
  url,
  commentary,
  list_item_tags
) => {
  let item = {
    id: Date.now(),
    title: title,
    username: username,
    password: password,
    url: url,
    commentary: commentary,
    list_item_tags: list_item_tags,
  };
  arrayItems.push(item);
  return item;
};

const saveArrayItems = () => {
  localStorage.setItem("Items", JSON.stringify(arrayItems));
  viewItem();
};

const viewItem = () => {
  registered_items.innerHTML = "";

  arrayItems = JSON.parse(localStorage.getItem("Items"));
  if (arrayItems === null) {
    arrayItems = [];
  } else {
    arrayItems.forEach((element) => {
      registered_items.innerHTML += `<tr><td>${element.title}</td><td>${element.username}</td><td>${element.password}</td><td>${element.url}</td><td>${element.commentary}</td><td>${element.list_item_tags}</td><td><i class="material-icons float-left mr-2">mode_edit</i><i class="material-icons float-left mr-2">delete</i></td></tr>`;
    });
  }
};

const deleteItem = (item) => {
  let indexArray;
  arrayItems.forEach((element, index) => {
    if (element.title === item) {
      indexArray = index;
      console.log(index);
    }
  });
  arrayItems.splice(indexArray, 1);
  saveArrayItems();
};

const editItem = (item) => {
  let indexArray = arrayItems.findIndex((element) => element.title === item);
  // console.log(indexArray);
  // console.log(arrayItems[indexArray]);
  arrayItems[indexArray].title = "9Do2to";
  saveArrayItems();
};

form_add_item.addEventListener("submit", (e) => {
  e.preventDefault();
  let title = document.querySelector("#title").value;
  let username = document.querySelector("#username").value;
  let password = document.querySelector("#password").value;
  let url = document.querySelector("#url").value;
  let commentary = document.querySelector("#commentary").value;
  let list_item_tags = document.querySelector("#list_item_tags").value;

  console.log(username, password, url, commentary, list_item_tags);

  newItem(title, username, password, url, commentary, list_item_tags);
  saveArrayItems();

  form_add_item.reset();
});

//Este evento inicia cuando se carga el DOM
document.addEventListener("DOMContentLoaded", viewItem);

registered_items.addEventListener("click", (e) => {
  e.preventDefault();
  console.log(e.path[2].childNodes[1].innerHTML);
  if (e.target.innerHTML === "delete" || e.target.innerHTML === "mode_edit") {
    let item = e.path[2].childNodes[1].innerHTML;
    if (e.target.innerHTML === "delete") {
      deleteItem(item);
    }
    if (e.target.innerHTML === "mode_edit") {
      editItem(item);
    }
  }
});
